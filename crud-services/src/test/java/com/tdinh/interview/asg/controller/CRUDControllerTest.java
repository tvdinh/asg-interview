package com.tdinh.interview.asg.controller;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Matchers.anyInt;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.tdinh.interview.asg.AsgInterviewApplication;
import com.tdinh.interview.asg.entity.Project;
import com.tdinh.interview.asg.entity.ProjectType;
import com.tdinh.interview.asg.service.CRUDService;
import com.tdinh.interview.asg.service.CRUDServiceException;

/**
 * Unit test class for {@link CRUDController}
 * 
 * @author Tuan V. Dinh
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = {AsgInterviewApplication.class})
@AutoConfigureMockMvc
public class CRUDControllerTest {

	@Mock
	private CRUDService mockCRUDService;
	
	@InjectMocks
	private CRUDController controllerUnderTest; 
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();
	}

	@Test
	public void test_getProjectById_thenReturnCorrectOne() throws Exception {

		Integer projectId = 1;
		Project aProject = new Project.Builder().projectType(ProjectType.DOCS_MANAGEMENT).name("Project X").build();

		when(mockCRUDService.view(projectId)).thenReturn(aProject);

		mvc.perform(get("/view")
			.param("projectId", "1"))
			.andDo(MockMvcResultHandlers.print())
			.andExpect(status().isOk());
	}

	@Test
	public void test_getProjectWithInvalidId_thenReturnBadRequest() throws Exception {

		mvc.perform(get("/view").param("projectId", "Invalid-Project-ID")).andExpect(status().isBadRequest());
	}
	
	@Test
	public void testview_whenErrorInCRUDService_returnServerErrorHttpCode() throws Exception {
		when(mockCRUDService.view(anyInt())).thenThrow(CRUDServiceException.class);
		mvc.perform(get("/view")
				.param("projectId", "1"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().is5xxServerError());
	}
	
	@Test
	public void test_removeProjectById_successful() throws Exception {
		Integer projectId = 1;
		doNothing().when(mockCRUDService).remove(projectId);

		mvc.perform(post("/remove")
			.param("projectId", "1"))
			.andDo(MockMvcResultHandlers.print())
			.andExpect(status().isAccepted());
	}
	
	@Test
	public void test_removeProjectWithInvalidId_thenReturnBadRequest() throws Exception {

		mvc.perform(post("/remove").param("projectId", "Invalid-Project-ID")).andExpect(status().isBadRequest());
	}
	
	@Test
	public void testremove_whenErrorInCRUDService_returnServerErrorHttpCode() throws Exception {
		Integer projectId = 1;
		doThrow(CRUDServiceException.class).when(mockCRUDService).remove(projectId);
		mvc.perform(post("/remove")
				.param("projectId", "1"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().is5xxServerError());
	}
	
	@Test
	public void testSaveProject_succesfull() throws Exception {

		String jsonBody = "{" +
			    "\"name\" : \"Project E for company B\"," +
			    "\"requestedDate\" : \"2018-04-30 18:00:00.000\"," +
			    "\"requiredDate\" : \"2018-10-30 18:00:00.000\"," +
			    "\"contactFirstName\" : \"John\"," +
			    "\"contactLastName\" : \"Doe\"," +
			    "\"contactPhone\" : \"0123456625\"," +
			    "\"contactEmail\" : \"john.doe@nowhere.com\"," +
			    "\"contactRole\" : \"manager\"," +
			    "\"contactTeam\" : \"ASG\"," +
			    "\"critical\" : \"true\"," +
			    "\"projectType\" : \"SECURITY\"}";
		
		mvc.perform(post("/addProject")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonBody))
				.andExpect(status().isAccepted());
	}
	
	@Test
	public void testSaveProject_errorBody_returnBadRequest() throws Exception {
		String errorJsonBody = "{" +
			    "\"name\" : \"Project E for company B\",}"; //extra comma
		
		mvc.perform(post("/addProject")
				.contentType(MediaType.APPLICATION_JSON)
				.content(errorJsonBody))
				.andExpect(status().isBadRequest());
	}
}
