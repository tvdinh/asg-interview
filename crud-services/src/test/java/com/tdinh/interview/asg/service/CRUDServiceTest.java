package com.tdinh.interview.asg.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.tdinh.interview.asg.entity.Project;
import com.tdinh.interview.asg.repository.ProjectRepository;

/**
 * Unit test class for {@link CRUDService}
 * @author Tuan V. Dinh
 *
 */
public class CRUDServiceTest {

	@Mock
	private Project mockProject, mockTargetProject;
	
	@Mock
	private ProjectRepository mockProjectRepository;
	
	@InjectMocks
	private CRUDService classUnderTest;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testSaveProjectService() throws Exception {
		when(mockProjectRepository.save(mockProject)).thenReturn(mockTargetProject);
		when(mockTargetProject.getProjectId()).thenReturn(1);
		
		Integer projectId= classUnderTest.save(mockProject);

		assertEquals(projectId, new Integer(1));
		verify(mockProjectRepository, times(1)).save(mockProject);
	}
	
	@Test
	public void testRemoveProjectService() throws Exception {
		doNothing().when(mockProjectRepository).deleteById(1);
		
		classUnderTest.remove(1);
		
		verify(mockProjectRepository, times(1)).deleteById(1);
	}
	
	@Test
	public void testViewProjectService() throws Exception {
		when(mockProjectRepository.findByProjectId(1)).thenReturn(mockTargetProject);
		
		classUnderTest.view(1);
		
		verify(mockProjectRepository, times(1)).findByProjectId(1);
	}
}
