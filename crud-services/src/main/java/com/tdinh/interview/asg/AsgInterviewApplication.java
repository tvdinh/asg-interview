package com.tdinh.interview.asg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.tdinh.interview.asg")
@EntityScan("com.tdinh.interview.asg")
public class AsgInterviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsgInterviewApplication.class, args);
	}
}
