package com.tdinh.interview.asg.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdinh.interview.asg.entity.Project;
import com.tdinh.interview.asg.repository.ProjectRepository;

/**
 * Service layer that perform project management services
 * @author Tuan V. Dinh
 *
 */
@Service
public class CRUDService {

	@Autowired
	private ProjectRepository projectRepository;
	
	public Integer save(Project project) throws CRUDServiceException {
		try {
			Project targetProject = projectRepository.save(project);
			return targetProject.getProjectId();
		} catch(Exception ex) {
			throw new CRUDServiceException();
		}
	}
	
	public Project view(Integer projectId) throws CRUDServiceException {
		try {
			return projectRepository.findByProjectId(projectId);
		} catch(Exception ex) {
			throw new CRUDServiceException();
		}
	}
	
	public void remove(Integer projectId) throws CRUDServiceException {
		try {
			projectRepository.deleteById(projectId);
		} catch(Exception ex) {
			throw new CRUDServiceException();
		}
	}
}
