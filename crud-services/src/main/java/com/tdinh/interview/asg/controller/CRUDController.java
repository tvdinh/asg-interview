package com.tdinh.interview.asg.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdinh.interview.asg.entity.Project;
import com.tdinh.interview.asg.repository.ProjectRepository;
import com.tdinh.interview.asg.service.CRUDService;
import com.tdinh.interview.asg.service.CRUDServiceException;

/**
 * Main controller for CRUD services. This controller uses a {@link CRUDService} which 
 * represents a service layer for project management operations, and hides the actual
 * Data layer implementation from the Web layer (i.e regardless of framework used in Data
 * layer, spring data or hibernate, etc.). In case changes required in data layer, this would not
 * be affected.
 * <p>
 *  
 * 
 * @author Tuan V. Dinh
 */
@RestController
public class CRUDController implements ErrorController {

	private static final String ERROR_PATH = "/error";
	private static final Logger log = Logger.getLogger(CRUDController.class.getName());
	
	@Autowired
	private CRUDService crudService;
	
	private ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * View an existing project.
	 * @param projectId
	 * @param map
	 * @return
	 */
	@GetMapping("/view")
	@ResponseBody
	public ResponseEntity<String> viewProject(@RequestParam("projectId") String projectId, Model map) {
		log.log(Level.FINE, "Query for project with Id: {0}", projectId);
		try {
			Integer id = Integer.valueOf(projectId);
			Project project = crudService.view(id);
			return ResponseEntity.ok(toJSON(project));
		} catch (NumberFormatException ex) {
			log.log(Level.SEVERE, "Error with projectId: {0}", ex.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		} catch (CRUDServiceException | JsonProcessingException ex) {
			log.log(Level.SEVERE, "Error: {0}", ex.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Remove an existing project
	 * @param projectId
	 * @param map
	 * @return
	 */
	@PostMapping("/remove")
	public @ResponseBody ResponseEntity<Boolean> removeProject(@RequestParam("projectId") String projectId, Model map) {
		log.log(Level.FINE, "Remove with Id: {0}", projectId);
		try {
			Integer id = Integer.valueOf(projectId);
			crudService.remove(id);
			return new ResponseEntity<Boolean>(HttpStatus.ACCEPTED);
		} catch (NumberFormatException ex) {
			log.log(Level.SEVERE, "Error with projectId: {0}", ex.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		} catch (CRUDServiceException ex) {
			log.log(Level.SEVERE, "Failed to remove project. Exception: {0}. Message {1}", 
					new Object [] {ex.getClass().getSimpleName(),ex.getMessage()});
			return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Register a new or update an existing project
	 * @param project
	 * @return
	 */
	@PostMapping("/addProject")
	public @ResponseBody ResponseEntity<Boolean> saveProject(
			@RequestBody String project) {
		log.log(Level.FINE, "Insert/Update a project. Body {0}", project);
		try {
			Integer projectId = crudService.save(fromJSON(project));
			log.log(Level.INFO, "Project is created/updated. Id = {0}", projectId);
			return new ResponseEntity<Boolean>(HttpStatus.ACCEPTED);
		} catch (CRUDServiceException | IOException ex) {
			log.log(Level.SEVERE, "Failed to insert or update project. Exception: {0}. Message {1}",  
					new Object [] {ex.getClass().getSimpleName(),ex.getMessage()});
			return new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(ERROR_PATH)
	public String error(Model model) {
		log.info("Some error occurs");
		return "error";
	}

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}
	
	private Project fromJSON(String project) throws IOException {
		return objectMapper.readValue(project, Project.class);
	}
	
	private String toJSON(Project project) throws JsonProcessingException {
		return objectMapper.writeValueAsString(project);
	}
}
