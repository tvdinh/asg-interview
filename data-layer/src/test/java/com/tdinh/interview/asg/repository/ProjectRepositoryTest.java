package com.tdinh.interview.asg.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.tdinh.interview.asg.entity.Project;


/**
 * Unit test for {@link ProjectRepository} 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/META-INF/application-context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection={"dbexample"})
@DatabaseSetup("/META-INF/projects-db.xml")
public class ProjectRepositoryTest {
	
	
	@Autowired
	private ProjectRepository projectRepository;
	
	/**
	 * Test  {@link ProjectRepository#findByProjectId()}
	 */
	@Test
	public void testFindByProjectId() throws Exception {
		Project targetProject = projectRepository.findByProjectId(1);
		assertNotNull(targetProject);
		assertEquals(targetProject.getName(),"Project X for company Y");
		assertEquals(targetProject.getContactFirstName(),"John");
		assertEquals(targetProject.getContactLastName(),"Doe");
	}
}
