package com.tdinh.interview.asg.repository;

import javax.inject.Named;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tdinh.interview.asg.entity.Project;

/**
 * A repository to manage CRUD services for {@link Project}s
 * @author Tuan V. Dinh
 *
 */
@Repository
@Named
public interface ProjectRepository extends JpaRepository<Project, Integer>  {

	/**
	 * Find a {@link Project} by project ID
	 * @return
	 */
	public Project findByProjectId(Integer projectId);
}
