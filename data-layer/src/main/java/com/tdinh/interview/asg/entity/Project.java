package com.tdinh.interview.asg.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Entity represents a Project
 * @author Tuan V. Dinh
 *
 */
@Entity
@Table(name = "project")
public class Project implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PROJECT_ID")
	private Integer projectId;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "SUMMARY")
	private String summary;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "REQUESTED_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestedDate;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "REQUIRED_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date requiredDate;

	@Column(name = "CONTACT_FIRST_NAME", nullable = false)
	private String contactFirstName;

	@Column(name = "CONTACT_LAST_NAME", nullable = false)
	private String contactLastName;

	@Column(name = "CONTACT_PHONE", nullable = false)
	private String contactPhone;

	@Column(name = "CONTACT_EMAIL", nullable = false)
	private String contactEmail;

	@Column(name = "CONTACT_ROLE", nullable = false)
	private String contactRole;

	@Column(name = "CONTACT_TEAM", nullable = false)
	private String contactTeam;

	@Column(name = "ESTIMATION")
	private Integer estimation;

	@Column(name = "PROJECT_TYPE")
	@Enumerated(EnumType.STRING)
	private ProjectType projectType;

	@Column(name = "IS_CRITICAL")
	private boolean isCritical = false;

	public Project() {
	}

	public Project(String name, String description, String summary, Date requestedDate, Date requiredDate,
			String contactFirstName, String contactLastName, String contactPhone, String contactEmail,
			String contactRole, String contactTeam, Integer estimation, ProjectType projectType, boolean isCritical) {
		super();
		this.name = name;
		this.description = description;
		this.summary = summary;
		this.requestedDate = requestedDate;
		this.requiredDate = requiredDate;
		this.contactFirstName = contactFirstName;
		this.contactLastName = contactLastName;
		this.contactPhone = contactPhone;
		this.contactEmail = contactEmail;
		this.contactRole = contactRole;
		this.contactTeam = contactTeam;
		this.estimation = estimation;
		this.projectType = projectType;
		this.isCritical = isCritical;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Date getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactRole() {
		return contactRole;
	}

	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}

	public String getContactTeam() {
		return contactTeam;
	}

	public void setContactTeam(String contactTeam) {
		this.contactTeam = contactTeam;
	}

	public Integer getEstimation() {
		return estimation;
	}

	public void setEstimation(Integer estimation) {
		this.estimation = estimation;
	}

	public ProjectType getProjectType() {
		return projectType;
	}

	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}

	public boolean isCritical() {
		return isCritical;
	}

	public void setCritical(boolean isCritical) {
		this.isCritical = isCritical;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public static class Builder {
		private String name;
		private String description;
		private String summary;
		private Date requestedDate;
		private Date requiredDate;
		private String contactFirstName;
		private String contactLastName;
		private String contactPhone;
		private String contactEmail;
		private String contactRole;
		private String contactTeam;
		private Integer estimation;
		private ProjectType projectType;
		private boolean isCritical;

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder description(String description) {
			this.description = description;
			return this;
		}

		public Builder summary(String summary) {
			this.summary = summary;
			return this;
		}

		public Builder requestedDate(Date requestedDate) {
			this.requestedDate = requestedDate;
			return this;
		}

		public Builder requiredDate(Date requiredDate) {
			this.requiredDate = requiredDate;
			return this;
		}

		public Builder contactFirstName(String contactFirstName) {
			this.contactFirstName = contactFirstName;
			return this;
		}

		public Builder contactLastName(String contactLastName) {
			this.contactLastName = contactLastName;
			return this;
		}

		public Builder contactPhone(String contactPhone) {
			this.contactPhone = contactPhone;
			return this;
		}

		public Builder contactEmail(String contactEmail) {
			this.contactEmail = contactEmail;
			return this;
		}

		public Builder contactRole(String contactRole) {
			this.contactRole = contactRole;
			return this;
		}

		public Builder contactTeam(String contactTeam) {
			this.contactTeam = contactTeam;
			return this;
		}

		public Builder estimation(Integer estimation) {
			this.estimation = estimation;
			return this;
		}

		public Builder projectType(ProjectType projectType) {
			this.projectType = projectType;
			return this;
		}

		public Builder isCritical(boolean isCritical) {
			this.isCritical = isCritical;
			return this;
		}

		public Project build() {
			return new Project(this);
		}
	}

	private Project(Builder builder) {
		this.name = builder.name;
		this.description = builder.description;
		this.summary = builder.summary;
		this.requestedDate = builder.requestedDate;
		this.requiredDate = builder.requiredDate;
		this.contactFirstName = builder.contactFirstName;
		this.contactLastName = builder.contactLastName;
		this.contactPhone = builder.contactPhone;
		this.contactEmail = builder.contactEmail;
		this.contactRole = builder.contactRole;
		this.contactTeam = builder.contactTeam;
		this.estimation = builder.estimation;
		this.projectType = builder.projectType;
		this.isCritical = builder.isCritical;
	}
}
