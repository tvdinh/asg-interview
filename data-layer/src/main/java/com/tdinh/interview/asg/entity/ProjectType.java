package com.tdinh.interview.asg.entity;

public enum ProjectType {
	DOCS_MANAGEMENT,
	SECURITY,
	COLLATERAL
}
